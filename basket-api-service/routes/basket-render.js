const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);

var schema = {
  "type": "array",
  "minItems": 1,
  "maxItems": 5,
  "items": {
  type: 'object',
  properties: {
  country: {
  type: "string",
  chance: "country"
  },
  date: {
  type: "string",
  chance: "date"
  },
  type: {
  
    "type": "string",
    "pattern": "star|tinsel|bow|rain"
  
  },
  
  percent: {
    "type": "integer",
    "minimum": 1,
    "maximum": 20,
    "exclusiveMinimum": true
   },
  coins1: {
    "type": "integer",
    "minimum": 5,
    "maximum": 100,
    "exclusiveMinimum": true
   },
  
  sentence: {
  type: 'string',
  chance: 'sentence'
  },
  company: {
  type: "company",
  chance: "company"
  },
  image: {
  type: "string",
  faker: "image.business"
  }
  
  
  },
  required: ['type','lastname', 'date', 'percent', 'company', 'image', 'sentence', 'coins1']
  }
  
  
  };





/* GET users listing. */
router.get('/', (req, res) => {
    jsf.resolve(schema).then(sample =>{
     res.render('basket', {sampleBasket: sample});
    });


  
});
/* {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },
    {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },*/
module.exports = router;
