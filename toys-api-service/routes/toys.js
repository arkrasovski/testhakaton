const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);

var schema = {
  "type": "array",
  "minItems": 1,
  "maxItems": 3,
  "items": {
  type: 'object',
  properties: {

  toyname: {

   "type": "string",
  "pattern": "star|tinsel|bow|rain"

  },

  coins1: {
       "type": "integer",
       "minimum": 10,
       "maximum": 120,
       "exclusiveMinimum": true
      },

sentence: {
  type: "string",
  chance: "sentence"
  },

  image: {
  type: "string",
  faker: "image.business"
  }
  },

 



  required: ['toyname', 'coins1','image',  'sentence']
  }


  };





/* GET users listing. */
router.get('/', (req, res) => {
   jsf.resolve(schema).then(sample =>{
    res.send(sample);
   });




});
/* {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },
    {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },*/
module.exports = router;